import { Component, OnInit, ViewChild, Output, EventEmitter  } from '@angular/core';
import { AlbumSelectComponent } from '../album-select/album-select.component';
import { FormGroup, FormControl } from '@angular/forms';
import { Post } from 'src/app/shared/models/post';
import { PostsService } from 'src/app/services/api/posts.service';

@Component({
  selector: 'app-post-editor',
  templateUrl: './post-editor.component.html',
  styleUrls: ['./post-editor.component.css']
})
export class PostEditorComponent implements OnInit {

  constructor(private postService: PostsService) { }
  albumToAdd: any;

  postEditorForm = new FormGroup({
    message: new FormControl('')
  });

  @ViewChild(AlbumSelectComponent, {static: true}) modalForAlbumSelect: AlbumSelectComponent;
  @Output() addedPostEmiter = new EventEmitter<string>();
  
  ngOnInit() {
  }
  
  onSubmit() {
    let post = new Post(
      this.postEditorForm.value.message,
      this.albumToAdd
    ); 
    this.postService.addPost(post).subscribe(res => {
      this.addedPostEmiter.emit('postAdded');
    });
  }

  openSelectAlbumModal() {
    this.modalForAlbumSelect.open();
  }

  onAlbumSelect(album: any) {
    this.albumToAdd = album;
  }

}
