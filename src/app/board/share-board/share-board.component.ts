import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { PostsService } from '../../services/api/posts.service';
import { FormGroup, FormControl } from '@angular/forms';
import { TokenStorageService } from '../../authorization/auth/token-storage.service';
import { Router } from '@angular/router';
import { AlbumPreviewComponent } from '../album-preview/album-preview.component';
import { AlbumSelectComponent } from '../album-select/album-select.component';
import { Comment } from 'src/app/shared/models/comment';
import { UserService } from 'src/app/authorization/services/user.service';

@Component({
  selector: 'app-share-board',
  templateUrl: './share-board.component.html',
  styleUrls: ['./share-board.component.css']
})
export class ShareBoardComponent implements OnInit {
  @Input() selectedAlbum: any;
  
  posts: any = [];
  albumToAdd: any;
  previewAlbum: any;
  showCommentSection: any;
  showCommentsSection: any;
  comments: any = [];
  userId: any;

  commentEditorForm = new FormGroup({
    commentMessage: new FormControl('')
  });

  @ViewChild(AlbumSelectComponent, {static: true}) modalForAlbumSelect: AlbumSelectComponent;
  @ViewChild(AlbumPreviewComponent, {static: true}) modalAlbumPreview: AlbumPreviewComponent;

  constructor(
    private postService: PostsService, 
    private tokenStorageService: TokenStorageService, 
    private router: Router,
    private userService: UserService) { }

  ngOnInit() {
    if(!this.tokenStorageService.getUsername()){
      this.router.navigate(['/home']);
    }
    this.postService.getPosts().subscribe(data => {
      this.posts = data;
    });
    this.userService.getUserInfo().subscribe(data => {
      this.userId = data['id'];
    })
    
  }

  openAlbumPreviewModal(album) {
    this.modalAlbumPreview.open(album);
  }

  updatePostsList() {
    this.postService.getPosts().subscribe(data => {
      this.posts = data;
    });
  }

  commentClick(postIndex) {
    this.commentEditorForm.setValue({
      commentMessage: ''
    });
    if(this.showCommentSection != postIndex){
      this.showCommentSection = postIndex;
      this.postService.getComments(this.posts[postIndex].id).subscribe(data => {
        this.comments[postIndex] = data;
      });
    } else {
      this.showCommentSection = -1;
    }
  }

  addComment(postIndex) {
    let comment = new Comment(
      this.commentEditorForm.value.commentMessage
    );
    this.postService.addComment(this.posts[postIndex].id, comment).subscribe(res => {
      this.postService.getComments(this.posts[postIndex].id).subscribe(data => {
        this.comments[postIndex] = data;
      });
    });
  }

  likePost(postIndex) {
    this.postService.likePost(this.posts[postIndex].id).subscribe(res => {
      this.posts[postIndex].totalLikes = res;
    });
  }

  deletePost(postIndex) {
    this.postService.deletePost(this.posts[postIndex].id).subscribe(res => {
      this.updatePostsList();
    });
  }
}
