import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumSelectComponent } from './album-select.component';

describe('AlbumSelectComponent', () => {
  let component: AlbumSelectComponent;
  let fixture: ComponentFixture<AlbumSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
