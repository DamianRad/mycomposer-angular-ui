import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlbumService } from 'src/app/services/api/album.service';
import { Album } from 'src/app/shared/models/album';

@Component({
  selector: 'app-album-select',
  templateUrl: './album-select.component.html',
  styleUrls: ['./album-select.component.css']
})
export class AlbumSelectComponent implements OnInit {

  @ViewChild('modalForAlbumSelect', {static: true}) modalForAlbumSelect: ElementRef;

  constructor(private modalService: NgbModal, private albumService: AlbumService) { }

  modalRef: NgbModalRef;
  albums: any = [];
  selectedAlbum: any;

  @Output() selectedAlbumEvent = new EventEmitter<Album>();

  ngOnInit() {
    this.albumService.getAlbums().subscribe(data => {
      this.albums = data;
    });
  }

  open() {
    this.modalRef = this.modalService.open(this.modalForAlbumSelect, { size: 'lg', centered: true, backdrop: 'static', keyboard: false });
  }

  onAlbumSelect(album) {
    this.selectedAlbum = album;
  }

  submitSelectedAlbum() {
    this.selectedAlbumEvent.emit(this.selectedAlbum);
    this.modalRef.close();
  }
}
