import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PostsService } from 'src/app/services/api/posts.service';
import { ApiMusicPlayerService } from 'src/app/services/api/api-music-player.service';

@Component({
  selector: 'app-album-preview',
  templateUrl: './album-preview.component.html',
  styleUrls: ['./album-preview.component.css']
})
export class AlbumPreviewComponent implements OnInit {

  constructor(private modalService: NgbModal, private postService: PostsService, private musicPlayerService: ApiMusicPlayerService) { }

  previewAlbum: any;
  songs: any = [];
  modalRef: NgbModalRef;
  musicFile: any;
  showSongDetails: any = false;
  showMusicPlayer: any = false;
  currentSelectedSong: any = -1;

  @ViewChild('modalAlbumPreview', { static: true }) modalAlbumPreview: ElementRef;
  ngOnInit() {
  }

  open(album) {
    this.previewAlbum = album;
    this.modalRef = this.modalService.open(this.modalAlbumPreview, { size: 'lg', centered: true, backdrop: 'static', keyboard: false });
    this.postService.getSongs(album.id).subscribe(data => {
      this.songs = data;
    });
  }

  onSongSelect(songIndex) {
    this.currentSelectedSong = songIndex;
    if (this.songs[songIndex].musicFileName) {
      this.musicFile = this.musicPlayerService.getMusicFileForSong(this.previewAlbum.id, this.songs[songIndex].id).subscribe(res => {
        this.showMusicPlayer = true;
      });
    } else {
      this.showMusicPlayer = false;
    }
  }
}
