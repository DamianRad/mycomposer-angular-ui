import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AlbumService } from '../services/api/album.service';
import { SongService } from '../services/api/song.service';
import { Album } from '../shared/models/album';
import { DataService } from '../shared/data/data.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlbumEditorComponent } from '../album-editor/album-editor.component';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  albums: any = [];
  errorMessage: string;
  selectedAlbum: any;
  albumToDelete: any;
  modalRef: NgbModalRef;
  albumSearchValue: string = "";

  @ViewChild(AlbumEditorComponent, {static: true}) modal: AlbumEditorComponent;
  @ViewChild('confirmationModalForAlbum', {static: true}) confirmationModal: ElementRef;

  constructor(private albumService: AlbumService, private songService: SongService, private dataService: DataService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getAlbumsList();
  }

  onSelect(album: any): void {
    this.selectedAlbum = album;
    this.dataService.changeAlbumId(this.selectedAlbum.id);
  }

  deleteAlbum(): void {
    this.albumService.deleteAlbum(this.albumToDelete.id).subscribe(() => {
      this.getAlbumsList();
      this.modalRef.close();
    });
  }

  getAlbumsList(): void {
    this.albumService.getAlbums().subscribe(
      data => {
        this.albums = data;
        if(data.length > 0) {
          this.selectedAlbum = data[0];
          this.dataService.changeAlbumId(this.selectedAlbum.id);
        } else {
          this.selectedAlbum = null;
        }
      },
      error => {
        this.errorMessage = `${error.status}: ${JSON.parse(error.error).message}`;
      }
    );
  }

  openAlbumEditor(album: any): void {
    this.modal.open(album);
  }

  openModalForCreateAlbum(): void {
    this.modal.openCreateAlbumModal();
  }

  openDeleteConfirmationModal(album: Album) {
    this.albumToDelete = album;
    this.modalRef = this.modalService.open(this.confirmationModal, { size: 'lg', centered: true, backdrop: 'static', keyboard: false });
  }
}
