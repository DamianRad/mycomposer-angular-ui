import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges, SimpleChange } from '@angular/core';
import { MusicPlayerService } from "../services/music/music-player.service";
import { ApiMusicPlayerService } from "../services/api/api-music-player.service";
import { StreamState } from "../interfaces/stream-state";
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-music-player',
  templateUrl: './music-player.component.html',
  styleUrls: ['./music-player.component.css']
})
export class MusicPlayerComponent implements OnInit {
  @Output() public selectedMusicFile = new EventEmitter<any>();
  @Input() public songId: any;
  @Input() public albumId: any;
  @Input() public isPreviewMode: any;

  files: Array<any> = [];
  state: StreamState;
  currentFile: any = {};
  songMusicFile: any;
  songMusicSrc: any;
  songMusicTitle: string;
  showMusicPlayer: boolean = false;
  @Input() public selectedSong: any;

  constructor(
    public musicPlayerService: MusicPlayerService,
    public apiMusicPlayerService: ApiMusicPlayerService,
    public sanitizer: DomSanitizer
  ) {
    this.musicPlayerService.getState().subscribe(state => {
      this.state = state;
    });
  }

  ngOnInit() {
    if (this.selectedSong && this.selectedSong.musicFileName != null) {
      this.apiMusicPlayerService.getMusicFileForSong(this.albumId, this.songId).subscribe(response => {
        this.createMusicFromBlob(response);
        this.songMusicTitle = this.selectedSong.musicFileName;
      });
    } else {
      this.selectedSong = null;
      this.songMusicFile = null;
      this.songMusicSrc = null;
      this.songMusicTitle = null;
      this.showMusicPlayer = false;
    }
  }

  isFirstPlaying() {
    return this.currentFile.index === 0;
  }

  isLastPlaying() {
    return this.currentFile.index === this.files.length - 1;
  }

  playStream(url) {
    this.musicPlayerService.playStream(url).subscribe(events => { });
  }

  openFile(file, index) {
    this.currentFile = { index, file };
    this.musicPlayerService.stop();
    this.playStream(file);
  }

  pause() {
    this.musicPlayerService.pause();
  }

  play() {
    if (this.isPreviewMode) {
      this.openFile(this.files[0], 0);
    } else {
      this.musicPlayerService.play();
    }
  }

  stop() {
    this.musicPlayerService.stop();
  }

  next() {
    const index = this.currentFile.index + 1;
    const file = this.files[index];
    this.openFile(file, index);
  }

  previous() {
    const index = this.currentFile.index - 1;
    const file = this.files[index];
    this.openFile(file, index);
  }

  onSliderChangeEnd(change) {
    this.musicPlayerService.seekTo(change.value);
  }

  onMusicSelected(event) {
    this.files[0] = event.target.files[0];
    this.songMusicTitle = event.target.files[0].name;
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.songMusicSrc = this.sanitizer.bypassSecurityTrustUrl(event.target.result);
      this.createMusicFromBlob(this.files[0]);
    }
    reader.readAsDataURL(this.files[0]);
  }

  createMusicFromBlob(music: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", (event: any) => {
      this.songMusicFile = reader.result;
      this.songMusicSrc = this.sanitizer.bypassSecurityTrustUrl(this.songMusicFile);
      this.files[0] = this.songMusicFile;
      this.selectedMusicFile.emit(music);
      this.showMusicPlayer = true;
    }, false);

    if (music) {
      reader.readAsDataURL(music);
    }
  }
}
