import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../authorization/auth/token-storage.service';
import { DataService } from '../shared/data/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private roles: string[];
  private authority: string;
  private loggedIn: boolean;
  private loggingOut: boolean = false;

  constructor(private tokenStorage: TokenStorageService, private data: DataService, private router: Router) {
  }

  ngOnInit() {
    this.data.isUserLogged.subscribe(isUserLogged => this.loggedIn = isUserLogged);
    this.data.userAuthority.subscribe(authority => this.authority = authority);
    if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every(role => {
        if (role === "ROLE_ADMIN") {
          this.authority = 'admin';
          this.data.changeUserLoginStatus(true);
          return false;
        } else {
          this.authority = 'user';
          this.data.changeUserLoginStatus(true);
          return true;
        }
      });
      }
  }

  logout() {
   this.loggingOut = true;
   if(!this.tokenStorage.getUsername()){
      this.data.changeUserLoginStatus(false);
   }

   this.tokenStorage.signOut();
   window.location.reload();
  }

}
