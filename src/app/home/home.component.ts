import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../authorization/auth/token-storage.service';
import { Router } from '@angular/router';
import { DataService } from '../shared/data/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  info: any;

  constructor(private token: TokenStorageService, private router: Router, private data: DataService) { }

  ngOnInit() {
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthorities()
    }
  }
}
