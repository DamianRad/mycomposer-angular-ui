import { Component, OnInit, Input, SimpleChanges, SimpleChange, ViewChild, EventEmitter, ElementRef } from '@angular/core';
import { SongService } from '../services/api/song.service';
import { Song } from '../shared/models/song';
import { SongEditorComponent } from '../song-editor/song-editor.component';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../shared/data/data.service';

@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.css']
})
export class SongComponent implements OnInit {
  @Input() album: any;
  albumId: number;
  songs: any;
  songToDelete: any;
  errorMessage: string;
  modalRef: NgbModalRef;
  albumTitle: string;
  songSearchValue: string = "";
  @ViewChild(SongEditorComponent, {static: true}) modal: SongEditorComponent;
  @ViewChild('confirmationModal', {static: true}) confirmationModal: ElementRef;

  constructor(private songService: SongService, private modalService: NgbModal, private data: DataService) { }

  ngOnInit() {
    this.data.currentAlbumId.subscribe(albumId => this.albumId = albumId);
  }

  openDeleteConfirmationModal(song: Song) {
    this.songToDelete = song;
    this.modalRef = this.modalService.open(this.confirmationModal, { size: 'lg', centered: true, backdrop: 'static', keyboard: false });
  }

  deleteSong(): void {
    this.songService.deleteSong(this.album.id, this.songToDelete.id).subscribe(()=>{
      this.getSongsList();
      this.modalRef.close();
    });
  }

  getSongsList() {
    this.songService.getSongs(this.album.id).subscribe(
      data => {
        this.songs = data;
      },
      error => {
        this.errorMessage = `${error.status}: ${JSON.parse(error.error).message}`;
      }
     );
  }

  ngOnChanges(changes: SimpleChanges) {
    const currentItem: SimpleChange = changes['album'];
    if(currentItem.currentValue){
      this.getSongsList();
      this.albumTitle = this.album.title.toUpperCase();
    } else {
      this.songs = [];
    }

  }

  openCreateSongEditor() {
   this.modal.openCreateSongEditor();
  }

  openUpdateSongEditor(song: Song) {
   this.modal.openUpdateSongEditor(song);
  }
}

