import { TestBed } from '@angular/core/testing';

import { ApiMusicPlayerService } from './api-music-player.service';

describe('ApiMusicPlayerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiMusicPlayerService = TestBed.get(ApiMusicPlayerService);
    expect(service).toBeTruthy();
  });
});
