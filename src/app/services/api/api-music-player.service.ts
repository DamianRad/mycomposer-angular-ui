import { Injectable } from '@angular/core';
import { of, Observable } from "rxjs";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiMusicPlayerService {

  constructor(private http: HttpClient) {}

  private getSongsPrefixUrl = 'http://localhost:8080/api/albums/';

  getMusicFileForSong(albumId: number, songId: number) : Observable<Blob>{
    return this.http.get(this.getSongsPrefixUrl + albumId + '/songs/' + songId + '/music', { responseType: 'blob'});
  }

  addMusicFileForSong(albumId: number) : Observable<any>{
    return this.http.post(this.getSongsPrefixUrl + albumId + '/songs', { responseType: 'json'});
  }
}
