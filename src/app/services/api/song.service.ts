import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Song } from '../../shared/models/song';

@Injectable({
  providedIn: 'root'
})
export class SongService {
  private getSongsPrefixUrl = 'http://localhost:8080/api/albums/';
  constructor(private http: HttpClient) { }

  addSong(song: Song, albumId: number, songMusicFile: any): Observable<any> {
    const formData = new FormData();
    if (songMusicFile) {
      var musicFile = this.convertBlobToFile(songMusicFile, songMusicFile.name);
      formData.append('songMusicFile', musicFile, musicFile.name);
    }
    formData.append('songData', new Blob([JSON.stringify(song)], { type: "application/json" }));
    return this.http.post<Song>(this.getSongsPrefixUrl + albumId + '/songs', formData, { responseType: 'json' });
  }

  updateSong(song: Song, albumId: number, songId: number, songMusicFile: any): Observable<any> {
    const formData = new FormData();
    if (songMusicFile) {
      var musicFile = this.convertBlobToFile(songMusicFile, songMusicFile.name);
      formData.append('songMusicFile', musicFile, musicFile.name);
    }
    formData.append('songData', new Blob([JSON.stringify(song)], { type: "application/json" }));
    return this.http.put<Song>(this.getSongsPrefixUrl + albumId + '/songs/' + songId, formData, { responseType: 'json' });
  }

  getSongs(albumId: number): Observable<any> {
    return this.http.get(this.getSongsPrefixUrl + albumId + '/songs', { responseType: 'json' });
  }

  deleteSong(albumId: number, songId: number): Observable<any> {
    return this.http.delete(this.getSongsPrefixUrl + albumId + '/songs/' + songId)
  }

  convertBlobToFile(blobImage, fileName: string): File {
    return new File([new Blob([blobImage], { type: 'application/json' })], fileName);
  }
}
