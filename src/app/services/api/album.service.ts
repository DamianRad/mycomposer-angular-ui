import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Album } from '../../shared/models/album';

const HttpUploadOptions = {
  headers: new HttpHeaders({ "Accept": "application/json" })
}

@Injectable({
  providedIn: 'root'
})
export class AlbumService {
  private albumsUrl = 'http://localhost:8080/api/albums';

  constructor(private http: HttpClient) { }

  addAlbum(album: any, selectedImage): Observable<any> {
    const formData = new FormData();
    if(selectedImage) {
      formData.append('albumImage', selectedImage, selectedImage.name);
    }
    formData.append('albumData', new Blob([JSON.stringify(album)], { type: "application/json"}));
    return this.http.post(this.albumsUrl, formData, HttpUploadOptions);
  }

  getAlbums(): Observable<any> {
    return this.http.get(this.albumsUrl, { responseType: 'json'} );
  }

  getAlbumImage(albumId: number): Observable<Blob> {
    return this.http.get(this.albumsUrl + '/' + albumId + '/image', { responseType: 'blob'} );
  }

  updateAlbum(album: Album, albumId: number, selectedImage): Observable<any> {
    const formData = new FormData();
    if(selectedImage) {
      var imageFile = this.convertBlobToFile(selectedImage, selectedImage.name);
      formData.append('albumImage', imageFile, imageFile.name);
    }
    formData.append('albumData', new Blob([JSON.stringify(album)], { type: "application/json" }));
    return this.http.put(this.albumsUrl + '/' + albumId, formData, HttpUploadOptions );
  }

  deleteAlbum(id: number): Observable<any> {
    return this.http.delete(this.albumsUrl + '/' + id);
  }

  convertBlobToFile(blobImage, fileName: string): File {
    return new File([new Blob([blobImage], { type: 'application/json'})], fileName);
  }
}
