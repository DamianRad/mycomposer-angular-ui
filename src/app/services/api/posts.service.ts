import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Post } from 'src/app/shared/models/post';
import { Observable } from 'rxjs';
import { Comment } from 'src/app/shared/models/comment';

const HttpUploadOptions = {
  headers: new HttpHeaders({ "Accept": "application/json" })
}

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) { }

  url: string = 'http://localhost:8080/api/posts';

  addPost(post: Post): Observable<any> {
    return this.http.post(this.url, post, HttpUploadOptions);
  }

  getPosts(): Observable<any>{
    return this.http.get(this.url);
  }

  getSongs(albumId): Observable<any>{
    return this.http.get('http://localhost:8080/api/albums/' + albumId + '/songs')
  }

  getComments(postId) : Observable<any>{
    return this.http.get(this.url + '/'+ postId +'/comment');
  }

  addComment(postId, comment: Comment) : Observable<any>{
    return this.http.post(this.url + '/'+ postId +'/comment', comment, HttpUploadOptions);
  }

  likePost(postId) : Observable<any>{
    return this.http.post(this.url + '/like', {'postId':postId}, HttpUploadOptions)
  }

  deletePost(postId) : Observable<any> {
    return this.http.delete(this.url + '/' + postId);
  }
}
