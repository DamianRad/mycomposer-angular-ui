export class User {
  name: string;
  email: string;
  imageName: string;

  constructor(name: string, email: string, imageName: string) {
    this.name = name;
    this.email = email;
    this.imageName = imageName;
  }
}
