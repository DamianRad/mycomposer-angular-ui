import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';

const HttpUploadOptions = {
  headers: new HttpHeaders({ "Accept": "application/json" })
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userUrl = 'http://localhost:8080/api/user';
  private adminUrl = 'http://localhost:8080/api/admin';

  constructor(private http: HttpClient) { }

  getUserInfo(): Observable<any> {
    return this.http.get(this.userUrl, { responseType: 'json'} );
  }

  getUserImage(): Observable<any> {
    return this.http.get(this.userUrl + "/image", { responseType: 'blob'} );
  }

  getAdminBoard(): Observable<string> {
    return this.http.get(this.adminUrl, { responseType: 'text'} );
  }

  updateUser(userInfo): Observable<any> {
    return this.http.put(this.userUrl, userInfo, HttpUploadOptions);
  }

  uploadUserImage(selectedImage): Observable<any> {
      const formData = new FormData();
      if(selectedImage) {
        var imageFile = this.convertBlobToFile(selectedImage, selectedImage.name);
        formData.append('userImage', imageFile, imageFile.name);
      }
      return this.http.post(this.userUrl + '/image', formData, HttpUploadOptions);
  }

  convertBlobToFile(blobImage, fileName): File {
    return new File([new Blob([blobImage], { type: 'application/json'})], fileName);
  }

}
