import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { FormControl, FormGroup } from '@angular/forms';
import { TokenStorageService } from '../auth/token-storage.service';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { User } from '../models/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  userData: string;
  errorMessage: string;
  selectedTab: string = 'userBoard';
  selectedImage: any = null;
  selectedImagePath: string = "";
  imageSrc: any;
  updateUserFailed: any;
  isUpdateComplete: any = false;

  userForm = new FormGroup({
    username: new FormControl(''),
    name: new FormControl(''),
    email: new FormControl('')
  });

  constructor(private userService: UserService, private tokenStorageService: TokenStorageService, private router: Router, private sanitizer: DomSanitizer) { }

    ngOnInit() {
      if(!this.tokenStorageService.getUsername()){
        this.router.navigate(['/home']);
      }
      this.userService.getUserInfo().subscribe(
        data => {
          this.userData = data;
          this.userForm.setValue({
            name: this.userData['name'],
            username: this.userData['username'],
            email: this.userData['email']
          });
          if(this.userData['imageName']) {
           this.userService.getUserImage().subscribe(
            response => {
              this.createImageFromBlob(response);
            }
           );
          }
        },
        error => {
          this.errorMessage = `${error.status}: ${JSON.parse(error.error).message}`;
        }
      );
    }

    onImageSelected(event) {
      this.selectedImage = event.target.files[0];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.imageSrc = this.sanitizer.bypassSecurityTrustUrl(event.target.result);
        this.selectedImagePath = event.target.result;
      }
      reader.readAsDataURL(this.selectedImage);
      this.userService.uploadUserImage(this.selectedImage).subscribe();
    }

    createImageFromBlob(image: Blob) {
      let reader = new FileReader();
      reader.addEventListener("load", (event: any) => {
        this.selectedImage = reader.result;
        this.imageSrc = this.sanitizer.bypassSecurityTrustUrl(this.selectedImage);
      }, false);

      if (image) {
        reader.readAsDataURL(image);
      }
    }

    onSubmit() {
      let user = new User(
       this.userForm.value.name,
       this.userForm.value.email,
       this.userData['imageName']
      );
      this.isUpdateComplete = true;
      this.userService.updateUser(user).subscribe(
        data => {
          this.isUpdateComplete = false;
        },
        error => {
          this.isUpdateComplete = false;
          this.updateUserFailed = true;
          this.errorMessage = error.error.message;
        }
      );
    }
  }

