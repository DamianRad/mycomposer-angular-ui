import { Album } from './album';

export class Post {
    constructor(
      public message: string,
      public album: Album
    ) {}
  }
  