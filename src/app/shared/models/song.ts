export class Song {
  constructor(
    public title: string,
    public text: string,
    public author: string
  ) {}
}
