import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, searchValue: string): any {
    if (searchValue === "") {
      return value;
    }
    const copyArray: any[] = [];
    for (let i = 0; i < value.length; i++) {
      let title: string = value[i].title;
      if (title.toLocaleLowerCase().match(searchValue)) {
        copyArray.push(value[i]);
      }
    }
    return copyArray;
  }

}
