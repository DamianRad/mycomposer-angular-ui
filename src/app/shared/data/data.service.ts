import { Injectable, EventEmitter, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TokenStorageService } from '../../authorization/auth/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private albumSource = new BehaviorSubject<number>(0);
  private isUserLoggedSource = new BehaviorSubject<boolean>(false);
  private userAuthoritySource = new BehaviorSubject<string>('');
  currentAlbumId = this.albumSource.asObservable();
  isUserLogged = this.isUserLoggedSource.asObservable();
  userAuthority = this.userAuthoritySource.asObservable();

  constructor() {}

  changeAlbumId(albumId: number) {
    this.albumSource.next(albumId);
  }

  changeUserLoginStatus(isUserLogged: boolean) {
    this.isUserLoggedSource.next(isUserLogged);
  }

  changeUserAuthority(userAuthority: string) {
    this.userAuthoritySource.next(userAuthority);
  }
}
