import { Component, OnInit, ViewChild, Output, SimpleChanges, SimpleChange, EventEmitter, ViewContainerRef, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SongService } from '../services/api/song.service';
import { Song } from '../shared/models/song';
import { DataService } from '../shared/data/data.service';
import { MusicPlayerService } from '../services/music/music-player.service';

@Component({
  selector: 'app-song-editor',
  templateUrl: './song-editor.component.html',
  styleUrls: ['./song-editor.component.css']
})
export class SongEditorComponent implements OnInit {
  albumId: number;
  songId: number;

  isEditModeOpen: boolean = false;
  openModal: boolean = false;

  modalRef: NgbModalRef;
  modalTitle: string;
  submitBtnLabel: string;
  songMusicFile: any;
  selectedSong: any;

  @Output() addedSongEvent = new EventEmitter<Song>();
  @ViewChild('content', {static: false}) content: ViewContainerRef;

  songForm = new FormGroup({
    author: new FormControl(''),
    title: new FormControl(''),
    text: new FormControl('')
  });

  constructor(
    private modalService: NgbModal, 
    public activeModal: NgbActiveModal, 
    private songService: SongService, 
    private data: DataService, 
    public musicPlayerService: MusicPlayerService) { }

  ngOnInit() {
    this.data.currentAlbumId.subscribe(albumId => this.albumId = albumId);
  }

  openCreateSongEditor() {
    this.songMusicFile = null;
    this.modalTitle = 'Create song';
    this.submitBtnLabel = 'Add song';
    this.setFormData('','','');
    this.isEditModeOpen = false;
    this.musicPlayerService.resetState();
    this.modalRef = this.modalService.open(this.content, { size: 'lg', centered: true, backdrop: 'static', keyboard: false });
  }

  openUpdateSongEditor(song) {
    this.modalTitle = 'Update song';
    this.submitBtnLabel = 'Update song';
    this.songId = song.id;
    this.selectedSong = song;
    this.setFormData(song.author, song.title, song.text);
    this.isEditModeOpen = true;
    this.musicPlayerService.resetState();
    this.modalRef = this.modalService.open(this.content, { size: 'lg', centered: true, backdrop: 'static', keyboard: false });
  }

  onSubmit() {
    let song = new Song(
      this.songForm.value.title,
      this.songForm.value.text,
      this.songForm.value.author
    );
    if(!this.isEditModeOpen) {
      this.songService.addSong(song, this.albumId, this.songMusicFile).subscribe(response => {
       this.isEditModeOpen = false;
       this.addedSongEvent.emit(song);
       this.modalRef.close();
      });
    } else {
      this.songService.updateSong(song, this.albumId, this.songId, this.songMusicFile).subscribe(response => {
        this.isEditModeOpen = false;
        this.addedSongEvent.emit(song);
        this.modalRef.close();
      });
    }
  }

  setFormData(author: string, title: string, text: string) {
    this.songForm.setValue({
      author: author,
      title: title,
      text: text
    });
  }

  closeModal() {
    this.isEditModeOpen = false;
    this.selectedSong = null;
    this.musicPlayerService.stop();
    this.modalRef.close();
  }

  onMusicFile(file: any) {
    this.songMusicFile = file;
  }
}
