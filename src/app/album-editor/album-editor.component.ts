import { Component, OnInit, ViewChild, ViewContainerRef, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup } from '@angular/forms';
import { AlbumService } from '../services/api/album.service';
import { Album } from '../shared/models/album';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-album-editor',
  templateUrl: './album-editor.component.html',
  styleUrls: ['./album-editor.component.css']
})
export class AlbumEditorComponent implements OnInit {
  album: any;
  modalTitle: string;
  modalRef: NgbModalRef;
  submitBtn: string;
  isEditModeOpen: boolean = false;

  imageFile: any = null;
  isImageLoading: boolean = false;
  imageSrc: any = null;

  @ViewChild('content', {static: false}) content: ViewContainerRef;
  @Output() addedAlbumEvent = new EventEmitter<Album>();
  constructor(private modalService: NgbModal, public activeModal: NgbActiveModal, private albumService: AlbumService, private sanitizer: DomSanitizer) { }

  albumForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl('')
  });

  ngOnInit() {
  }

  open(album) {
    this.setUpEditor(album);
    this.modalRef = this.modalService.open(this.content, { size: 'lg', centered: true, backdrop: 'static', keyboard: false });
  }

  setUpEditor(album) {
    this.modalTitle = 'Update album';
    this.submitBtn = 'Update album';
    this.album = album;
    this.isEditModeOpen = true;
    this.loadAlbumImage(album);
    this.setFormData(this.album.title, this.album.description ? this.album.description : '');
  }

  loadAlbumImage(album) {
    if(this.album.imagePath) {
        this.isImageLoading = true;
        this.albumService.getAlbumImage(album.id).subscribe(response => {
          this.createImageFromBlob(response);
          this.isImageLoading = false
        });
      } else {
        this.imageFile = null;
        this.imageSrc = null;
      }
  }

  openCreateAlbumModal() {
    this.imageFile = null;
    this.imageSrc = null;
    this.modalTitle = 'Create album';
    this.submitBtn = 'Add album';
    this.setFormData('','');
    this.modalRef = this.modalService.open(this.content, { size: 'lg', centered: true, backdrop: 'static', keyboard: false });
  }

  setFormData(title: string, description: string) {
    this.albumForm.setValue({
      title: title,
      description: description
    });
  }

  onSubmit() {
    let album = new Album(
      this.albumForm.value.title,
      this.albumForm.value.description,
      this.imageFile ? this.imageFile.name : null
    );
    if(!this.isEditModeOpen) {
      this.albumService.addAlbum(album, this.imageFile).subscribe(response => {
       this.isEditModeOpen = false;
       this.addedAlbumEvent.emit(album);
       this.modalRef.close()
      });
     } else {
      this.albumService.updateAlbum(album, this.album.id, this.imageFile).subscribe(response => {
        this.isEditModeOpen = false;
        this.addedAlbumEvent.emit(album);
        this.modalRef.close();
      });
    }
  }

  onImageSelected(event) {
    this.imageFile = event.target.files[0];

    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageSrc = this.sanitizer.bypassSecurityTrustUrl(event.target.result);
    }
    reader.readAsDataURL(this.imageFile);
  }

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", (event: any) => {
      this.imageFile = reader.result;
      this.imageSrc = this.sanitizer.bypassSecurityTrustUrl(this.imageFile);
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  deleteImage() {
    this.imageSrc = null;
    this.imageFile = null;
  }
}
