import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './authorization/login/login.component';
import { RegisterComponent } from './authorization/register/register.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './authorization/user/user.component';
import { AdminComponent } from './authorization/admin/admin.component';

import { httpInterceptorProviders } from './authorization/auth/auth-interceptor';
import { AlbumComponent } from './album/album.component';
import { SongComponent } from './song/song.component';
import { SongEditorComponent } from './song-editor/song-editor.component';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './header/header.component';
import { AlbumEditorComponent } from './album-editor/album-editor.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { MusicPlayerComponent } from './music-player/music-player.component';
import { FilterPipe } from './shared/pipes/filter.pipe';
import { ShareBoardComponent } from './board/share-board/share-board.component';
import { AlbumPreviewComponent } from './board/album-preview/album-preview.component';
import { AlbumSelectComponent } from './board/album-select/album-select.component';
import { PostEditorComponent } from './board/post-editor/post-editor.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UserComponent,
    AdminComponent,
    AlbumComponent,
    SongComponent,
    SongEditorComponent,
    HeaderComponent,
    AlbumEditorComponent,
    MusicPlayerComponent,
    FilterPipe,
    ShareBoardComponent,
    AlbumPreviewComponent,
    AlbumSelectComponent,
    PostEditorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [ httpInterceptorProviders, NgbActiveModal ],
  bootstrap: [AppComponent],
  entryComponents: [
    SongEditorComponent,
    AlbumEditorComponent
  ]
})
export class AppModule { }
